function [ output_args ] = GCS_stereo( image1,image2,image_out )
%GCS_STEREO Summary of this function goes here
%   Detailed explanation goes here
Il = imread(image1);
Ir = imread(image2);

pars=[];
pars.searchrange=[0,255];
[D,W,x,w,K] = gcs(Il,Ir,[],pars);

D=uint16(D);
D=max(D*255+0.5,0);
imwrite(D,image_out);

end

